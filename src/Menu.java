import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.*;

public class Menu {
	public Menu() throws Exception{
		while(true)
		{
			this.show();
		}
		
	}
	public void show() throws Exception
	{
		System.out.println("请选择一下指令：\n -l 返回行数 \n -x 启动图形界面 \n -w 返回单词数 \n -c 返回字符数  \n -s <匹配条件> 递归处理文件 \n -a 返回代码行，注释行，空行 \n -e <停用词文件名> 跳过处理停用词文件中的单词\n"
				+ "以上指令输入后需跟上要查找的文件名，默认输出文件为result \n 输入 -o 指定输出数据到指定文件" );
	
		InputData inputdata = new InputData();
		Operate oper = new Operate();
		//做输入检验
		String[] str =inputdata.getCommand("请按要求输入命令", "您未输入要查找文件名");
		List<String> list = Arrays.asList(str);
		if(list.contains("-x"))
		{
			HighFunction hf = new HighFunction();
			hf.creat();
					
		}
		else
		{
		Extend ex = new Extend();
		//初始化文件数组
		ex.fileArray(str); 
		File[] file=ex.getFile();
		//数组转换集合
		List<File> array = Arrays.asList(file);
		List<File> listf= new ArrayList(array);
		if(file!=null)
		{
			
			if(list.contains("-s"))
			{
			
				//List<File> listf = Arrays.asList(file);
				Iterator<File> iter = listf.iterator();
				while(iter.hasNext())
				{
					
					if(!iter.next().toString().contains(list.get(list.indexOf("-s")+1)))
					iter.remove();
				}
				
			}
			int wordNum=0;
			int charNum=0;
			int lineNum=0;
			int[] arr= {0,0,0};
			boolean arrFlag=false;
			if(list.contains("-a"))
				arrFlag = true;
			StringBuffer buf1= new StringBuffer();
			
			for(int i=0;i<listf.size();i++)
			{
				oper.setFile1( listf.get(i));
				StringBuffer buf= new StringBuffer("文件："+listf.get(i).toString()+"\r\n");
				for(int j=0;j<str.length-1;j++)
				{
					switch (str[j])
					{
					case "-l" :
						lineNum=oper.getLine();
						//buf.append(oper.getLine()+"\n");
						break;
					case "-w":
						if(list.contains("-e"))
						{
							wordNum=oper.getWordWithStop(list.get(list.indexOf("-e")+1));
						}
						else
						{
							wordNum=oper.getWord();
						}
						break;
					case "-a":
						int[] temp=oper.getA();
						arr[0]=temp[0];
						arr[1]=temp[1];
						arr[2]=temp[2];
						break;
					case "-c":
					charNum=oper.getChar();
						break;
					case "-o":
						oper.setFile2(new File(str[str.length-1]));break;
					default :
						break;
					}
				}
				if(wordNum!=0)buf.append("单词数为"+wordNum+"\r\n");
				if(charNum!=0)buf.append("字符数为"+charNum+"\r\n");
				if(lineNum!=0)buf.append("行数为"+lineNum+"\r\n");
				if(arrFlag)buf.append("代码行数为"+arr[0]+"\r\n空白行数为"+arr[1]+"\r\n注释行为"+arr[2]+"\r\n");
				buf1.append(buf);
				
				
			}
			
			oper.output(buf1);
			System.out.println("操作成功");
		}
		else
		{
			System.out.println("不存在此目录或文件");
		}
		
		}	
	}
}
