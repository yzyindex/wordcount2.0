import java.io.*;
import java.util.regex.Pattern;
import java.util.*;
public class Operate {
	private File file1;
	public File getFile1() {
		return file1;
	}
	public void setFile1(File file1) {
		this.file1 = file1;
	}
	private File file2;

	public File getFile2() {
		return file2;
	}
	public void setFile2(File file2) {
		this.file2 = file2;
	}
	private BufferedReader buf;
	private Writer write;
	public Operate() throws IOException {
		
		this.file2= new File("result.txt");
		
	}
	public int getWordWithStop(String file) throws Exception
	{
		this.buf=new BufferedReader(new InputStreamReader(new FileInputStream(this.file1)));
		File f= new File(file);
		BufferedReader buf1= new BufferedReader(new InputStreamReader(new FileInputStream(f)));
		String str =null;
		Pattern p = Pattern.compile(" |,");
		List<String> list = new ArrayList<String>();
		while((str=buf1.readLine())!=null)
		{
			String[] sarray = p.split(str);
			list.addAll(Arrays.asList(sarray));
			
			
		}
		String temp=null;
		List<String> list2 = new ArrayList<String>();
		while((temp=buf.readLine())!=null)
		{
			String[] str2 =p.split(temp);
			list2.addAll(Arrays.asList(str2));
		}
		list2.removeAll(list);
		
		return list2.size() ;
	}
	public int getChar() throws IOException
	{
		this.buf=new BufferedReader(new InputStreamReader(new FileInputStream(this.file1)));
	
		String temp=null;
		int num=0; 
		while((temp=buf.readLine())!=null)
		{
			char[] c= temp.toCharArray();
			num+=c.length;
		}
		
		return num;
	}
	public int getWord() throws IOException
	{
		this.buf=new BufferedReader(new InputStreamReader(new FileInputStream(this.file1)));
		int wordNum=0;
		String temp=null;
		Pattern p = Pattern.compile(" |,");
		while((temp=buf.readLine())!=null)
		{
			
			String[] str2 =p.split(temp);
			wordNum+=str2.length;
		
		
		}
		
		
		return wordNum ;
	}
	public int getLine() throws IOException
	{
		this.buf=new BufferedReader(new InputStreamReader(new FileInputStream(this.file1)));
	
		String temp=null;
		int num=0;
		while((temp=buf.readLine())!=null)
		{
			num++;
		}
	
		return num;
	}
	public int[] getA()
	{
		
		
	    String s=null;
	    int array[] = {0,0,0};
	    try {
	    boolean comm = false;
	    this.buf=new BufferedReader(new InputStreamReader(new FileInputStream(this.file1)));
        while((s = buf.readLine()) != null) {
            if(s.startsWith("/*") && s.endsWith("*/")) {
             array[2]++;
            	// codeComments++;
            } else if(s.trim().startsWith("//")) {
                array[2]++;
            	//codeComments++;
            } else if(s.startsWith("/*") && !s.endsWith("*/")) {
               array[2]++;;
            	//codeComments++;
                comm = true;
            } else if(!s.startsWith("/*") && s.endsWith("*/")) {
               array[2]++;
            	//codeComments++;
                comm = false;
            } else if(comm) {
               array[2]++;
            	//codeComments++;
            } else if(s.trim().length() < 1) {
                array[1]++;
                
            	//codeBlank++;
            } else {
                array[0]++;
            	//code++;
            }
        }
       
        
        buf.close();
      
    } catch (FileNotFoundException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
	    return array;
	}
	public void output(StringBuffer buf) throws IOException
	{
		this.write=new FileWriter(this.file2);
		this.write.write(buf.toString());
		this.write.close();
	}
}
