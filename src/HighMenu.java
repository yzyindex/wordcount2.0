import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HighMenu {
	
	public boolean check(String[] str)
	{
		boolean flag=false;
		Pattern p1 = Pattern.compile("[lwcosaex]");//该正则表达式检验是否有非法参数输入
		Matcher ma =null;
		//命令的参数长度必定大于2
		if(str.length>=2)
		{
			
			
			//倒数第二位不-o,不指定输出文件
			 if(str[str.length-2].indexOf("-o")==-1)
			{
				
				int count=0;
				//此循环用于遍历命令数组，统计含-的命令数
				for(int i=0;i<str.length-1;i++)
				{
					ma= p1.matcher(str[i]);
					
					if(str[i].startsWith("-")&&str[i].length()==2)
					{
						if(str[i].contains("-s"))//若带—s参数必带一个匹配的字符，此时需要加1
						{
							count++;
						}
						if(str[i].contains("-e"))//若带-e参数必定带一个 停用表 ，此时需加1
						{
							count++;
						}
						if(ma.find())
						{
							count++;
						}
					}
					
				}
				if(count==(str.length-1))
					{
						flag=true; 
						return flag;
					}
				
				else
					return flag;
			}
			else //指定输出文件
			{
				int count=0;
				for(int i=0;i<str.length-1;i++)
				{
					ma= p1.matcher(str[i]);
					
					if(str[i].startsWith("-")&&str[i].length()==2)
					{
						if(str[i].contains("-s"))
						{
							count++;
						}
						if(str[i].contains("-e"))
						{
							count++;
						}
						if(ma.find())
						{
							count++;
						}
					}
					
				}
				if(count==(str.length-2))
					{
					flag=true;
					return flag;
				
					}
				else
					return flag;
				
			}
		
				
			}	
		else
		{
			return flag;
		}
		
	}
	public List<String> excute(String[] str) throws Exception
	{
		String flag;
		List<String> listRe= new ArrayList<String>();
		Operate oper = new Operate();
		List<String> list = Arrays.asList(str);
		Extend ex = new Extend();
		//初始化文件数组
		ex.fileArray(str); 
		File[] file=ex.getFile();
		//数组转换集合
		if(file.length!=0)
		{
			
		List<File> array = Arrays.asList(file);
		List<File> listf= new ArrayList(array);
		
		
			if(list.contains("-s"))
			{
			
				//List<File> listf = Arrays.asList(file);
				Iterator<File> iter = listf.iterator();
				while(iter.hasNext())
				{
					
					// 迭代列表，根据输入的文件匹配信息进行匹配
					if(!iter.next().toString().contains(list.get(list.indexOf("-s")+1)))
					iter.remove();
				}
				
			}
			if(listf.size()==0)
			{
				flag="2";
				listRe.add(flag);
				
			}
			else
			{
			int wordNum=0;
			int charNum=0;
			int lineNum=0;
			int[] arr= {0,0,0};
			boolean arrFlag=false;
			if(list.contains("-a"))
				arrFlag = true;
			StringBuffer buf1= new StringBuffer();
			
			for(int i=0;i<listf.size();i++)
			{
				oper.setFile1( listf.get(i));
				StringBuffer buf= new StringBuffer("文件："+listf.get(i).toString()+"\r\n");
				for(int j=0;j<str.length-1;j++)
				{
					switch (str[j])
					{
					case "-l" :
						lineNum=oper.getLine();
						//buf.append(oper.getLine()+"\n");
						break;
					case "-w":
						if(list.contains("-e"))
						{
							wordNum=oper.getWordWithStop(list.get(list.indexOf("-e")+1));
						}
						else
						{
							wordNum=oper.getWord();
						}
						break;
					case "-a":
						int[] temp=oper.getA();
						arr[0]=temp[0];
						arr[1]=temp[1];
						arr[2]=temp[2];
						break;
					case "-c":
					charNum=oper.getChar();
						break;
					case "-o":
						oper.setFile2(new File(str[str.length-1]));break;
					default :
						break;
					}
				}
				if(wordNum!=0)buf.append("单词数为"+wordNum+"\r\n");
				if(charNum!=0)buf.append("字符数为"+charNum+"\r\n");
				if(lineNum!=0)buf.append("行数为"+lineNum+"\r\n");
				if(arrFlag)buf.append("代码行数为"+arr[0]+"\r\n空白行数为"+arr[1]+"\r\n注释行为"+arr[2]+"\r\n");
				buf1.append(buf);
				System.out.println(buf1);
			}
			
			oper.output(buf1);
			System.out.println("操作成功");
			
			flag="1";
			listRe.add(flag);
			listRe.add(buf1.toString());
		
			}
		}
		else
		{
			System.out.println("文件夹为空");
			flag="0";
			listRe.add(flag);
		}
		return listRe;
	}
}
