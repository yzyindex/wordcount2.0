import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.regex.Pattern;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.util.*;
import java.util.List;

public class HighFunction {
	static StringBuffer buf= new StringBuffer();
	JCheckBox cb1 ;
	JCheckBox cb2 ;
	JCheckBox cb3 ;
	JCheckBox cb4 ;
	JCheckBox cb5 ;
	JCheckBox cb6 ;
	JCheckBox cb7;
	JTextField tf1 ;
	JButton button ;
	JButton button1;
	JButton button2;
	JTextField inputFile ;
	JTextField outputFile ;
	JTextField stopFile ;
	JTextArea show;
	JScrollPane jsp;
	JFrame jframe = new JFrame();
	JPanel jpanel = new JPanel();
	
	
		public void creat()
		{
			
			Container con = jframe.getContentPane();
		
			jpanel.setLayout(new FlowLayout());
			MyItemListener IL = new MyItemListener();
			MyActionListener AL = new MyActionListener();
			MyActionListener1 AL1 = new MyActionListener1();
			 cb1 = new JCheckBox("-l");
			cb2 = new JCheckBox("-w");
			cb3 = new JCheckBox("-c");
			 cb4 = new JCheckBox("-e");
			 stopFile = new JTextField("停用词表",30);
			 cb5 = new JCheckBox("-a");
			cb6 = new JCheckBox("-s");
			cb7 = new JCheckBox("-o");
			cb1.addItemListener(IL);
			cb2.addItemListener(IL);
			cb3.addItemListener(IL);
			cb4.addItemListener(IL);
			cb5.addItemListener(IL);
			cb6.addItemListener(IL);
			cb7.addItemListener(IL);
			tf1 = new JTextField("选择-s后可输入匹配条件",30);
			inputFile = new JTextField("输入文件名",30);
			outputFile = new JTextField("输出文件名",30);
			show= new JTextArea(20, 20);
			show.setVisible(false);
			//给show加滚动条
			 jsp= new JScrollPane(show);
			jsp.setVisible(false);
			button = new JButton("执行");
			button1 = new JButton("选择文件");
			button2 = new JButton("选择文件");
			button2.setEnabled(false);
			button.addActionListener(AL);
			button1.addActionListener(AL1);
			button2.addActionListener(AL1);
			outputFile.setEditable(false);
			tf1.setEditable(false);
			 stopFile.setEditable(false);
			 inputFile.setEditable(false);
			jpanel.add(cb1);
			jpanel.add(cb2);
			jpanel.add(cb3);
			jpanel.add(cb4);
			jpanel.add(cb5);
			jpanel.add(cb6);
			jpanel.add(cb7);
			jpanel.add(button);
			jpanel.add(tf1);
			jpanel.add(inputFile);
			jpanel.add(button1);
			jpanel.add(outputFile);
			jpanel.add(stopFile);
			jpanel.add(button2);
			jpanel.add(jsp);
			con.add(jpanel);
			jframe.setLocation(500, 500);
			jframe.setSize(500,500);
			jframe.setVisible(true);
			
			
		}
		class MyActionListener1 implements ActionListener
		{

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				File file = null;
				int result =0;
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				if(e.getSource()==button1)
				{
					inputFile.setText("");
					fileChooser.setApproveButtonText("确定");
					fileChooser.setDialogTitle("打开文件");
					result= fileChooser.showOpenDialog(jframe);
					if(result==JFileChooser.APPROVE_OPTION)
					{
						file=fileChooser.getSelectedFile();
						inputFile.setText(file.toString());
					}
				}
				if(e.getSource()==button2)
				{
					stopFile.setText("");
					fileChooser.setApproveButtonText("确定");
					fileChooser.setDialogTitle("打开文件");
					result= fileChooser.showOpenDialog(jframe);
					if(result==JFileChooser.APPROVE_OPTION)
					{
						file=fileChooser.getSelectedFile();
						stopFile.setText(file.toString());
					}
				}
			}
			
		}
		class MyActionListener implements ActionListener
		{

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				java.util.List<String> list = new ArrayList<String>();
				if(cb1.isSelected())
					list.add("-l");
				if(cb2.isSelected())
					list.add("-w");
				if(cb3.isSelected())
					list.add("-c");
				if(cb4.isSelected())
					//buf.append("-e "+stopFile.getText()+" ");
					{
					list.add("-e");
					list.add(stopFile.getText());
					}
					
				if(cb5.isSelected())
					list.add("-a");
				if(cb6.isSelected())
					//buf.append("-s "+tf1.getText()+" ");
				{
					list.add("-s");
					list.add(tf1.getText());
				}
				
				
					String input = inputFile.getText().trim();
					
					list.add(input);
					if(outputFile.isEditable())
					{
						//buf.append("-o "+outputFile.getText());
						list.add("-o");
						list.add(outputFile.getText());
					}
					String[] str = list.toArray(new String[list.size()]);
					for(String var:str)
					{
						System.out.println(var);
					}
					
					HighMenu menu = new HighMenu();
					if(menu.check(str))
					{
						List<String> listRe= new ArrayList<String>();
						try {
							listRe=menu.excute(str);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						if(listRe.get(0).equals("1"))
						{
							
							show.setText("");
							show.setText(listRe.get(1));
							show.setVisible(true);
							jsp.setVisible(true);
							JInternalFrame jif = new JInternalFrame("提示",false,true,false,false);
							JLabel lable = new JLabel("---成功----");
							jif.add(lable);
							jif.setSize(200, 200);
							
							jif.setVisible(true);
							jpanel.add(jif);
							buf.setLength(0);
							
							
						}
						
						if(listRe.get(0).equals("0"))
						{
							JInternalFrame jif = new JInternalFrame("提示",false,true,false,false);
							JLabel lable = new JLabel("---该目录为空----");
							jif.add(lable);
							jif.setSize(200, 200);
							jif.setVisible(true);
							jpanel.add(jif);
							buf.setLength(0);
						}
						if(listRe.get(0).equals("2"))
						{
							JInternalFrame jif = new JInternalFrame("提示",false,true,false,false);
							JLabel lable = new JLabel("---未找到匹配文件----");
							jif.add(lable);
							jif.setSize(200, 200);
							jif.setVisible(true);
							jpanel.add(jif);
							buf.setLength(0);
						}
						
					}
					else
					{
						JInternalFrame jif = new JInternalFrame("警告",false,true,false,false);
						JLabel lable = new JLabel("-----请选择参数 ----");
						jif.add(lable);
						jif.setSize(200, 200);
						jif.setVisible(true);
						jpanel.add(jif);
					}
					
				
				
				
			}
			
		}
		class MyItemListener implements ItemListener
		{

			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				JCheckBox cb = (JCheckBox) e.getSource();
				System.out.println(cb.getText().toString());
				if(cb.isSelected())
				{
					if(cb.getText().equals("-s"))
					{
						
						tf1.setEditable(true);
						tf1.setText("");
					}
					if(cb.getText().equals("-o"))
					{
						outputFile.setText("");
						outputFile.setEditable(true);
					}
					if(cb.getText().equals("-e"))
					{
						button2.setEnabled(true);
					}
				
				}
				else
				{
					if(cb.getText().equals("-s"))
					{
						tf1.setEditable(false);
						tf1.setText("选择-s后可输入匹配条件");
					}
					if(cb.getText().equals("-o"))
					{
						outputFile.setText("输出文件名");
						outputFile.setEditable(false);
					}
					if(cb.getText().equals("-e"))
					{
						button2.setEnabled(false);
						stopFile.setText("停用词表");;
					}
					
				
				}
				
					
				
			}
			
		}
}
