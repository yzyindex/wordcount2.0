import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class InputData {
	private BufferedReader buf;
	public InputData() {
		buf= new BufferedReader(new InputStreamReader(System.in));
	}

	//执行输入操作的函数
	public String getString(String infor)
	{
		System.out.println(infor);
		String str= null;
		try {
			str=buf.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	//检验 输入命令的正确性
	public String[] getCommand(String infor,String err)
	{
		String str[]= null;
		boolean flag=true;
		Pattern p = Pattern.compile(" +");//设置空格的正则表达式
		Pattern p1 = Pattern.compile("[lwcosaex]");//该正则表达式检验是否有非法参数输入
		Matcher ma =null;
		while(flag)//输入不正确 循环输入知道正确为止。
		{
			str= p.split(getString(infor));//将输入信息切分成数组
			//输出输入内容
			for(String val:str)
			{
				System.out.println(val); 
			}
			if(str[0].equals("-x"))
			{ 
				return str;
			}
			
			//命令的参数长度必定大于2
			else if(str.length>=2)
			{
				
				
				//倒数第二位不-o,不指定输出文件
				if(str[str.length-2].indexOf("-o")==-1)
				{
					
					int count=0;
					//此循环用于遍历命令数组，统计含-的命令数
					for(int i=0;i<str.length-1;i++)
					{
						ma= p1.matcher(str[i]);
						
						if(str[i].startsWith("-")&&str[i].length()==2)
						{
							if(str[i].contains("-s"))//若带—s参数必带一个匹配的字符，此时需要加1
							{
								count++;
							}
							if(str[i].contains("-e"))//若带-e参数必定带一个 停用表 ，此时需加1
							{
								count++;
							}
							if(ma.find())
							{
								count++;
							}
						}
						
					}
					if(count==(str.length-1))
						{
							flag=false; 
							//oper.setFile1(new File(str[str.length-1]));
						}
					
					else
						System.out.println("存在错误指令,请按要求输入");
				}
				else //指定输出文件
				{
					int count=0;
					for(int i=0;i<str.length-1;i++)
					{
						ma= p1.matcher(str[i]);
						
						if(str[i].startsWith("-")&&str[i].length()==2)
						{
							if(str[i].contains("-s"))
							{
								count++;
							}
							if(str[i].contains("-e"))
							{
								count++;
							}
							if(ma.find())
							{
								count++;
							}
						}
						
					}
					if(count==(str.length-2))
						{
						flag=false;
						//oper.setFile1(new File(str[str.length-3]));
						}
					else
						System.out.println("存在错误指令,请按要求输入");
					
				}
			
					
				}	
			else
			{
				System.out.println(err);
			}
		}
		return str;
	}
	
}
